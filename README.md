# KSF Bookmarklets
These bookmarklets make it easy to substitute Encycloreader for Wikipedia.

## Encycloreader Redirect
Install this bookmarklet to change the current Wikipedia article to the corresponding
Encycloreader article.  
Add this HTML to a page to instruct a user to install the bookmark:
```html
Drag this bookmark to your bookmark bar <a href="javascript:(function(){location=(''+location).replace('en.wikipedia.org/wiki/','encycloreader.org/r/wikipedia.php?q=')})();">View on ER</a>
```

## Encycloreader Transform
Install this bookmarklet to change all links on the current page from Wikipedia links to the
corresponding Encycloreader article.  
Add this HTML to a page to instruct a user to install the bookmark:
```html
Drag this bookmark to your bookmark bar <a href="javascript:(function(){l=document.getElementsByTagName('a');for(i=0;i<l.length;i++)l[i].href=l[i].href.replace('en.wikipedia.org/wiki/','encycloreader.org/r/wikipedia.php?q=')})();">Wiki to ER</a>
```

## License

GNU Lesser General Public License v3.0
https://www.gnu.org/licenses/lgpl-3.0.en.html

## Project status

Pre-Alpha
